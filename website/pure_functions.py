import hmac
import time

from django.conf import settings

def make_hashtag(last_name, first_name, patronymic):
    tag = last_name+first_name+patronymic
    hash = hmac.new(settings.SECRET_KEY.encode('utf-8'),
                    (tag+str(time.time())).encode('utf-8')).hexdigest()
    return tag+'#'+hash[:4]
