# Generated by Django 2.0.4 on 2018-04-18 17:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0008_onetimepassword'),
    ]

    operations = [
        migrations.AddField(
            model_name='onetimepassword',
            name='used',
            field=models.BooleanField(default=False),
        ),
    ]
