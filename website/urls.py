"""conference URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  path(r'^blog/', include('blog.urls'))
"""
from django.urls import path
from django.views.generic import TemplateView
from allauth.account.views import LoginView
from . import views, forms


urlpatterns = [
    path(r'', views.landing, name='landing'),
    path(r'become_jury', views.become_jury, name='become_jury'),
    path(r'become_participant', views.become_participant, name='become_participant'),
    path(r'download_passwords', views.download_passwords, name='download_passwords'),
    path(r'download_projects', views.download_projects, name='download_projects'),
    path(r'jury', views.jury, name='jury'),
    path(r'thank_you', views.thank_you, name='thank_you'),
]
