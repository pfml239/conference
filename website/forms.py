from allauth.account import forms as account_forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from django.urls import reverse_lazy
from django import forms

from .models import OneTimePassword

class BootstrapForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields.values():
            if isinstance(field.widget, forms.widgets.CheckboxInput):
                field.widget.attrs.update({'class': 'form-check-input'})
            else:
                field.widget.attrs.update({'class': 'form-control'})

    def is_valid(self):
        ret = super().is_valid()
        for f in self.errors:
            self.fields[f].widget.attrs.update({'class': self.fields[f].widget.attrs.get('class',
                                                                                         '') + \
                                                ' is-invalid'})
        return ret

class UserForm(BootstrapForm):
    def __init__(self, *args, **kwargs):
        readonly = kwargs.pop('readonly', False)
        super().__init__(*args, **kwargs)
        self.fields['last_name'].widget.attrs.update({'readonly': readonly})
        self.fields['first_name'].widget.attrs.update({'readonly': readonly})
        self.fields['patronymic'].widget.attrs.update({'readonly': readonly})
    last_name = forms.CharField(max_length=150)
    first_name = forms.CharField(max_length=30)
    patronymic = forms.CharField(max_length=30)
    last_name.widget.attrs['placeholder'] = 'Фамилия'
    first_name.widget.attrs['placeholder'] = 'Имя'
    patronymic.widget.attrs['placeholder'] = 'Отчество'

class JuryForm(UserForm):
    password = forms.CharField(max_length=30, widget=forms.PasswordInput())
    password.widget.attrs['placeholder'] = 'Пароль'

    def clean_password(self):
        password = OneTimePassword.objects.filter(password=self.cleaned_data['password'])
        if password:
            password = password.get()
            if not password.used:
                return password
        raise forms.ValidationError('Неверный пароль', code='invalid_password')

class ProjectForm(BootstrapForm):
    title = forms.CharField(label='Проект', max_length=255, required=False)
    enlist = forms.BooleanField(label='Зарегистрировать', required=False, initial=True)

class BaseProjectFormSet(forms.BaseFormSet):
    def clean(self):
        if any(self.errors):
            return
        _forms = [form for form in self.forms if form.cleaned_data['enlist']]
        if not _forms:
            raise forms.ValidationError('Выберите хотя бы один проект')
        if len({form.cleaned_data['title'] for form in _forms}) != len(_forms):
            raise forms.ValidationError('Один проект можно заявить только один раз')
        self.forms = _forms
        self.management_form.cleaned_data['TOTAL_FORMS'] = len(self.forms)

ProjectFormSet = forms.formset_factory(ProjectForm, formset=BaseProjectFormSet, extra=0)
