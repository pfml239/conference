import csv

from django.contrib.auth.decorators import login_required
from django.contrib.auth import login, logout
from django.contrib.auth.models import Group
from django.shortcuts import render, redirect
from django.http import HttpResponse

from .forms import UserForm, JuryForm, ProjectForm, ProjectFormSet
from .models import OneTimePassword, User, Project
from .decorators import user_is_a
from .utils import make_hashtag

@user_is_a('admin')
def download_passwords(request):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="passwords.csv"'

    writer = csv.writer(response)
    for p in OneTimePassword.objects.filter(used=False):
        writer.writerow([p.password])

    return response

@user_is_a('admin')
def download_projects(request):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="projects.csv"'

    writer = csv.writer(response)
    writer.writerow(['N', 'Автор(ы)', 'Название', 'Законченность/10', 'Стиль/10', 'Технологии/10',
                     'Алгоритмы/10', 'Выступление/10', 'Сумма'])
    for i, p in enumerate(Project.objects.filter(to_exhibit=True), start=1):
        writer.writerow([i, 
                         '\n'.join(' '.join([name.last_name, name.first_name, name.patronymic]) for name in p.authors.all()),
                         p.title])

    return response

@login_required
def user_profile(request):
    return render(request, 'user_profile.html', context={'user': request.user})

def thank_you(request):
    if request.user.is_authenticated:
        logout(request)
    request.session.flush()
    return render(request, 'thank_you.html')

def landing(request):
    if request.method == 'POST':
        form = UserForm(request.POST)
        if form.is_valid():
            if request.user.is_authenticated:
                logout(request)
            request.session.update(form.cleaned_data)
            if 'be_participant' in request.POST:
                return redirect('become_participant')
            elif 'be_jury' in request.POST:
                return redirect('become_jury')
    else:
        form = UserForm()
    ctx = {'user_form': form}
    return render(request, 'landing.html', context=ctx)

@user_is_a('jury')
def jury(request):
    return render(request, 'jury.html')

def become_jury(request):
    if request.method == 'POST':
        form = JuryForm(request.POST, readonly=True)
        if form.is_valid():
            password = form.cleaned_data['password']
            data = dict(form.cleaned_data)
            del data['password']
            data['username'] = make_hashtag(**data)
            user = User.objects.create(onetime=True, **data)
            user.groups.add(password.group)
            password.used = True
            password.user = user
            password.save()
            login(request, user, backend='django.contrib.auth.backends.ModelBackend')
            return redirect('jury')
        print([field.errors for field in form])            
    else:
        form = JuryForm(initial=request.session, readonly=True)
    ctx = {'jury_form': form}
    return render(request, 'become_jury.html', ctx)

def become_participant(request):
    if request.method == 'POST':
        user_form = UserForm(request.POST, readonly=True)
        project_formset = ProjectFormSet(request.POST)
        if user_form.is_valid() and project_formset.is_valid():
            user, created = User.objects.get_or_create(**request.session)
            if created:
                user.username = make_hashtag(**request.session)
                user.groups.add(Group.objects.get(name='participant'))
                user.onetime = True
                user.save()
            for project in project_formset.cleaned_data:
                p = Project.objects.filter(title=project['title'], authors=user)
                if len(p)==1:
                    p = p.get()
                else:
                    p = Project.objects.create(title=project['title'])
                    p.authors.add(user)
                p.to_exhibit = True
                p.save()
            return redirect('/thank_you')
    else:
        user_form = UserForm(initial=request.session, readonly=True)
        user, created = User.objects.get_or_create(**request.session)
        qs = Project.objects.filter(authors=user)
        if not created and qs:
            initial_projects = [{'title': project.title} for project in qs]
        else:
            initial_projects = [{'title': ''}]
        if created:
            user.delete()
        project_formset = ProjectFormSet(initial=initial_projects)
    ctx = {'user_form': user_form, 'project_formset': project_formset}
    return render(request, 'become_participant.html', ctx)
