import hmac
import time
import csv
from collections import defaultdict
from django.contrib.auth.models import Group
from django.conf import settings
from .models import User, Project
from .pure_functions import make_hashtag


def load_projects_from_csv(filename):
    with open(filename) as file:
        apps = [row for row in csv.DictReader(file)]
    _load_managers(apps)
    _load_solo_projects(apps)
    _load_collective_projects(apps)

def _load_collective_projects(apps):
    collective = list(filter(lambda x: x['collective'], apps))
    d = defaultdict(list)
    for app in collective:
        d[app['collective']] += [app]
    for _, value in d.items():
        p = Project.objects.create(title=value[0]['project_name'])
        m = dict(zip(('last_name', 'first_name', 'patronymic'), value[0]['manager'].split()))
        m = User.objects.get(**m)
        p.managers.add(m)
        for app in value:
            last_name, first_name, patronymic = app['surname'], app['name'], app['patronymic']
            u, created = User.objects.get_or_create(
                                    last_name=last_name,
                                    first_name=first_name,
                                    patronymic=patronymic)
            if created:
                u.username = make_hashtag(last_name, first_name, patronymic) 
                u.onetime = True
                u.is_active = False
                u.save()
            u.groups.add(Group.objects.get(name='participant'))
            p.authors.add(u)



def _load_solo_projects(apps):
    for app in filter(lambda x: not x['collective'], apps):
        p = Project.objects.create(title=app['project_name'])
        last_name, first_name, patronymic = app['surname'], app['name'], app['patronymic']
        u, created = User.objects.get_or_create(
                                last_name=last_name,
                                first_name=first_name,
                                patronymic=patronymic)
        if created:
            u.username = make_hashtag(last_name, first_name, patronymic) 
            u.onetime = True
            u.is_active = False
            u.save()
        u.groups.add(Group.objects.get(name='participant'))
        p.authors.add(u)
        m = dict(zip(('last_name', 'first_name', 'patronymic'), app['manager'].split()))
        m = User.objects.get(**m)
        p.managers.add(m)

def _load_managers(apps):
    users = []
    for manager in {app['manager'] for app in apps}:
        last_name, first_name, patronymic = manager.split()
        users.append(User(username=make_hashtag(last_name, first_name, patronymic),
                          last_name=last_name,
                          first_name=first_name,
                          patronymic=patronymic,
                          onetime=True,
                          is_active=False))
    User.objects.bulk_create(users)
    db_users = User.objects.all()
    if len(db_users)==len(users):
        for user in db_users:
            user.groups.add(Group.objects.get(name='manager'))
    else:
        raise ValueError('too many users in db to make them managers')
