from django.contrib import admin
from . import models
from guardian.admin import GuardedModelAdmin

# Register your models here.
admin.site.register(models.User)
admin.site.register(models.Project)
admin.site.register(models.OneTimePassword)
