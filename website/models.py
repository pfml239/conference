import random

from django.contrib.auth.models import AbstractUser, Group
from django.db import models

from .pure_functions import make_hashtag

class ConferenceStage(models.Model):
    """
    - Прием заявок
    - Обработка заявок
    - Набор жюри
    - Проведение
    - Оценка
    - Выдача дипломов
    """
    name = models.CharField(max_length=255)
    description = models.TextField()
    start = models.DateTimeField()
    end = models.DateTimeField()
    conference = models.ForeignKey('Conference', on_delete=models.CASCADE)
    def __str__(self):
        return self.name

class Conference(models.Model):
    title = models.CharField(max_length=255)
    description = models.TextField()
    def __str__(self):
        return self.title

class Section(models.Model):
    title = models.CharField(max_length=255)
    description = models.TextField()
    conference = models.ForeignKey('Conference', on_delete=models.CASCADE)
    def __str__(self):
        return self.title

class Criterion(models.Model):
    KIND = {
        1: "Float",
        2: "Boolean",
    }
    name = models.CharField(max_length=255)
    description = models.TextField()
    kind = models.SmallIntegerField(choices=KIND.items(), default=1)
    section = models.ForeignKey(Section, on_delete=models.CASCADE)

class Score(models.Model):
    criterion = models.ForeignKey(Criterion, on_delete=models.CASCADE)
    project = models.ForeignKey('Project', on_delete=models.CASCADE)
    jury = models.ForeignKey('User', on_delete=models.CASCADE)
    _boolean_value = models.BooleanField(default=False)
    _float_value = models.FloatField(default=0.0)

    @property
    def value(self):
        if Criterion.KIND[self.criterion.kind] == "Float":
            return self._float_value
        if Criterion.KIND[self.criterion.kind] == "Boolean":
            return self._boolean_value

    @value.setter
    def set_value(self, value):
        if Criterion.KIND[self.criterion.kind] == "Float":
            self._float_value = value
        if Criterion.KIND[self.criterion.kind] == "Boolean":
            self._boolean_value = value


class Room(models.Model):
    title = models.CharField(max_length=255)
    description = models.TextField()
    current_project = models.OneToOneField('Project', on_delete=models.SET_NULL, related_name='+', null=True)
    conference = models.ForeignKey('Conference', on_delete=models.CASCADE)
    def __str__(self):
        return self.title

class User(AbstractUser):
    patronymic = models.CharField(max_length=255, blank=True)
    onetime = models.BooleanField(default=False)

    def is_a(self, group_name):
        return self.groups.filter(name=group_name).exists()
    def get_full_name(self):
        return ' '.join([self.last_name, self.first_name, self.patronymic])
    def make_onetime(self, group):
        self.username = make_hashtag(self.last_name, self.first_name, self.patronymic)
        self.onetime = True
        self.save()
        self.groups.add(Group.objects.get(name=group))

class Project(models.Model):
    title = models.CharField(max_length=255)
    description = models.TextField(blank=True)
    link = models.URLField(blank=True)
    authors = models.ManyToManyField('ParticipantProfile', related_name='projects')
    managers = models.ManyToManyField(User, related_name='managers')
    room = models.ForeignKey(Room, on_delete=models.SET_NULL, null=True, default=None)
    section = models.ForeignKey(Section, on_delete=models.SET_NULL, null=True, default=None)
    # https://docs.djangoproject.com/en/2.2/ref/models/fields/#filefield
    #presentation = models.FileField()
    to_exhibit = models.BooleanField(default=False)
    scores = models.ManyToManyField(Criterion, through=Score)

    def __str__(self):
        return self.title

def find_last():
    if Sequence.objects.exists():
        return Sequence.objects.aggregate(models.Max('value'))['value__max'] + 1000 
    return 1000

class Sequence(models.Model):
    project = models.OneToOneField(Project, on_delete=models.CASCADE)
    value = models.BigIntegerField(default=find_last)

class ParticipantProfile(models.Model):
    school = models.CharField(max_length=255)
    city = models.CharField(max_length=255)
    grade = models.CharField(max_length=255)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

VOWELS = 'aeiou'
CONSONANTS = 'mpbfvntdszlrjwkg'
def make_onetime_password(syllables=4):
    return ''.join(''.join([random.choice(CONSONANTS), random.choice(VOWELS)]) for _ in
                   range(syllables))

def get_jury():
    return Group.objects.get(name='jury').pk

class OneTimePassword(models.Model):
    password = models.CharField(max_length=255, default=make_onetime_password)
    used = models.BooleanField(default=False)
    group = models.ForeignKey(Group, models.CASCADE, default=get_jury)
    user = models.ForeignKey(User, models.CASCADE, null=True, default=None, blank=True)
