from functools import wraps
from django.http import Http404

def user_is_a(group_name):
    def decorate(func):
        @wraps(func)
        def wrapper(request, *args, **kwargs):
            if request.user.is_authenticated and request.user.is_a(group_name):
                return func(request, *args, **kwargs)
            else:
                raise Http404
        return wrapper
    return decorate
