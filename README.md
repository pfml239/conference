# Конфа

## Что?

Веб-сервис для упрощения проведения конференций, в частности международной конференции "Школьная информатика и проблемы устойчивого развития" (название может поменяться)

## Как мне это у себя поставить?

```
git clone https://gitlab.com/pfml239/conference.git
cd conference
python3 -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt -r requirements_dev.txt
./manage.py migrate
./manage.py collectstatic
./manage.py runserver
```

Затем можно зайти на http://localhost:8000/ и обрадоваться.
