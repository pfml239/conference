    function checkEmailMatch() {
    var email = $("#inputEmail").val();
    var confirmEmail = $("#repeatEmail").val();

    if (email != confirmEmail)
        $("#checkEmail").html("Emails do not match!");
    else
        $("#checkEmail").html("Emails match.");
    }