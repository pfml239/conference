gunicorn
whitenoise
dj-database-url
django<3.0
django-allauth
django-crispy-forms
django-guardian
